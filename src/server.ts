import express from "express";
import bodyParser from "body-parser";
import logger from "./middlewares/logger";
import orderRoutes from "./controllers/order";
import userRoutes from "./controllers/user";
import productRoutes from "./controllers/product";
import jwtValidator from "./middlewares/jwtValidator";

const app: express.Application = express();
const port = 3000;

const middleware = [logger, bodyParser.json()];
app.use(middleware);

app.use("/orders", jwtValidator, orderRoutes);
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(port);

export default app;
