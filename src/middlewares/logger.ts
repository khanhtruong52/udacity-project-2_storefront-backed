import { Request, Response } from "express";

const logger  = (req: Request, _res: Response, next: () => void) => {
    console.log(req.url);
    next();
};

export default logger;