import Client from "../database";
import bcrypt from "bcrypt";

const saltRounds = process.env.SALT_ROUNDS || "0";
const pepper = process.env.PEPPER_KEY;

export type User = {
  id?: Number;
  firstName: string;
  lastName: string;
  password: string;
};

export class UserStore {
  async index(): Promise<User[]> {
    try {
      const sql = "SELECT * FROM users";
      const conn = await Client.connect();

      const result = await conn.query(sql);

      conn.release();

      return result.rows;
    } catch (err) {
      throw new Error(`${err}`);
    }
  }

  async show(id: string): Promise<User> {
    try {
      const sql = "SELECT * FROM users WHERE id=($1)";
      const conn = await Client.connect();

      const result = await conn.query(sql, [id]);

      conn.release();

      if (!result.rowCount) {
        throw `Could not find user ${id}.`;
      }

      return result.rows[0];
    } catch (err) {
      throw new Error(`${err}`);
    }
  }

  async create(user: User): Promise<User> {
    try {
      const sql =
        "INSERT INTO users (first_name, last_name, password_digest) VALUES($1, $2, $3) RETURNING *";
      const conn = await Client.connect();

      const hash = bcrypt.hashSync(
        user.password + pepper,
        parseInt(saltRounds)
      );

      const result = await conn.query(sql, [
        user.firstName,
        user.lastName,
        hash,
      ]);

      const order = result.rows[0];

      conn.release();

      return order;
    } catch (err) {
      throw new Error(`Could not add new user. Error: ${err}`);
    }
  }
}