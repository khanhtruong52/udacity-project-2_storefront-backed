import app from "../server";
import supertest from "supertest";
import jwt from "jsonwebtoken";
import { OrderStore } from "../models/order";
import { UserStore } from "../models/user";

const store = new OrderStore();
const userStore = new UserStore();

describe("Order Endpoints suite test", () => {
  let request;
  let user;
  const token = "Bearer " + jwt.sign({ user: {} }, process.env.JWT_SECRET_KEY);

  beforeAll(async () => {
    request = supertest(app);
    user = await userStore.create({
      firstName: "user",
      lastName: "A",
      password: "123",
    });

  });

  it("endpoint test for create", async () => {
    const response = await request
      .post("/orders")
      .send({
        userId: 1,
        status: "active"
      })
      .set("authorization", token);
    expect(response.status).toBe(200);
  });

  it("endpoint test for show", async () => {
    const response = await request
      .get("/orders/1")
      .set("authorization", token);
    expect(response.status).toBe(200);
  });
});

describe("Order Model suite test", () => {
  it("should have an show method", () => {
    expect(store.show).toBeDefined();
  });

  it("should have an create method", () => {
    expect(store.create).toBeDefined();
  });

  it("create method should add a product", async () => {

    const result = await store.create({
      userId: 1,
      status: "active",
    });

    expect(Number(result["user_id"])).toEqual(1);
  });
});
