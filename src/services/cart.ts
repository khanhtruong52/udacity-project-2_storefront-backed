import Client from "../database";
import { Order, OrderProducts } from "../models/order";

export class CartService {
  async addToCart(
    quantity: Number,
    orderId: Number,
    productId: Number
  ): Promise<OrderProducts> {
    try {
      const sql =
        "INSERT INTO order_products (quantity, order_id, product_id) VALUES($1, $2, $3) RETURNING *";
      const conn = await Client.connect();
      const result = await conn.query(sql, [quantity, orderId, productId]);

      const order = result.rows[0];

      conn.release();

      return order;
    } catch (error) {
      throw new Error(`Could not add new product into order. ${error}`);
    }
  }

  async getOrdersByUserId(userId: Number): Promise<Order[]> {
    try {
      const sql = "SELECT * FROM orders WHERE user_id=($1)";
      const conn = await Client.connect();
      const result = await conn.query(sql, [userId]);

      const ordersById = result.rows;

      conn.release();

      return ordersById;
    } catch (error) {
      throw new Error(`Could not get order(s) with this user id. ${error}`);
    }
  }
}
