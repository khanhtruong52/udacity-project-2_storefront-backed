# Storefront Backend Project

## Required Technologies
Your application must make use of the following libraries:
- Npm
- Docker
- Postman

## Getting Started

### 1. Run `yarn` to install all packages.

### 2. Database setting up instruction
#### Open terminal at project root location
> docker compose up
the postgres container will be started

#### Open another terminal
> docker ps -a, 
get the id/name of the current container we started

#### Access to Postgres's CLI
> docker exec -it <id/name> bash 

#### Login into postgres superuser
> su postgres
> psql postgres 

#### In psql run the following cmd to create role and database:
> CREATE USER full_stack_user WITH PASSWORD '12345';
> CREATE DATABASE full_stack_dev;
> CREATE DATABASE full_stack_test; //this one is for testing
> \c full_stack_dev
> GRANT ALL PRIVILEGES ON DATABASE full_stack_dev TO full_stack_user;

#### Open another terminal at project root location
> db-migrate up

#### Run below command in Postgres's CLI to check the schema
> \dt 
### 3. `yarn watch` to start the server

## Features
### 1. Jasmine test
> yarn test

Above command will run test suites.
*Note: Test DB need to be created before running this.

### 2. API Endpoints
*Required endpoints listed in README.md*
*Note: You can use Postman with localhost:3000 to test APIs.

## Port Information
- Node server: 3000
- Postgres: 5432

## Environment Variables
POSTGRES_HOST=0.0.0.0
POSTGRES_DB=full_stack_dev
POSTGRES_TEST_DB=full_stack_test
POSTGRES_USER=full_stack_user
POSTGRES_PASSWORD=12345
JWT_SECRET_KEY=truongnk3
SALT_ROUNDS=10
PEPPER_KEY=seasoning
ENV=dev