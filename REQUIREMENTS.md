## API Endpoints
#### Products
- Index: `/products/` - Get
- Show: `/products/:id` - Get
- Create [token required]: `/products/` - Post

#### Users
- Index [token required]: `/users/` - Get
- Show [token required]: `/users/:id` - Get
- Create N[token required]: `/users/` - Post

#### Orders
- Show: `/orders/:id` - Get
- Create:`/orders` - Post
- Current Order by user (args: user id)[token required]: `/orders/user/:id` - Get
- Add To Cart: `/orders/:id/product` - Post

## Database Schema
#### products
- id SERIAL PRIMARY KEY,
- name VARCHAR(50) NOT NULL,
- price integer NOT NULL

#### users
- id SERIAL PRIMARY KEY
- first_name VARCHAR(50) NOT NULL
- last_name VARCHAR(50) NOT NULL
- password_digest VARCHAR NOT NULL

#### orders
- id SERIAL PRIMARY KEY,
- status VARCHAR(10),
- user_id bigint REFERENCES users(id)

#### order_products
- id SERIAL PRIMARY KEY
- quantity integer
- order_id bigint REFERENCES orders(id)
- product_id bigint REFERENCES products(id)