import app from "../server";
import supertest from "supertest";
import jwt from "jsonwebtoken";
import { UserStore } from "../models/user";

const store = new UserStore();

describe("User Endpoints suite test", () => {
  let request;
  const token = "Bearer " + jwt.sign({ user: {} }, process.env.JWT_SECRET_KEY);

  beforeAll(() => {
    request = supertest(app);
  });

  it("endpoint test for index", async () => {
    const response = await request.get("/users").set("authorization", token);;
    expect(response.status).toBe(200);
  });

  it("endpoint test for create", async () => {
    const response = await request
      .post("/users")
      .send({
        firstName: "Jack",
        lastName: "Sparrow",
        password: "blackPearl",
      })
    expect(response.status).toBe(200);
  });

  it("endpoint test for show", async () => {
    const response = await request.get("/users/1").set("authorization", token);;
    expect(response.status).toBe(200);
  });
});

describe("User Model suite test", () => {
  it("should have an index method", () => {
    expect(store.index).toBeDefined();
  });

  it("should have an show method", () => {
    expect(store.show).toBeDefined();
  });

  it("should have an create method", () => {
    expect(store.create).toBeDefined();
  });

  it("create method should add a user", async () => {
    const result = await store.create({
      firstName: "Naruto",
      lastName: "Uzumaki",
      password: "rasengan",
    });

    expect(result["first_name"]).toEqual("Naruto");
  });

  it("index method should return a created user with correct last name", async () => {
    const result = await store.index();
    expect(result[2]["last_name"]).toEqual("Uzumaki");
  });

  it("show method should return the correct product", async () => {
    const result = await store.show("3");
    expect(result["first_name"]).toEqual("Naruto");
  });
});
