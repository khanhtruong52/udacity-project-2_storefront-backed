import app from "../server";
import supertest from "supertest";
import { CartService } from "../services/cart";
import jwt from "jsonwebtoken";

const cartService = new CartService();

describe("Cart Endpoints suite test", () => {
  let request;
  const token = "Bearer " + jwt.sign({ user: {} }, process.env.JWT_SECRET_KEY);

  beforeAll(() => {
    request = supertest(app);
  });

  it("endpoint test for get ordersById", async () => {
    const response = await request
      .get("/orders/user/1")
      .set("authorization", token);
    expect(response.status).toBe(200);
  });

  it("endpoint test for addToCart", async () => {
    const response = await request
      .post("/orders/1/product")
      .send({
        productId: 1,
        quantity: 5
      })
      .set('accept', 'application/json')
      .set("authorization", token);
    expect(response.status).toBe(500);
  });
});

describe("Cart Service method suite test", () => {
  it("should have an show method", () => {
    expect(cartService.getOrdersByUserId).toBeDefined();
  });

  it("should have an show method", () => {
    expect(cartService.addToCart).toBeDefined();
  });

  it("get orders method should return an empty list", async () => {
    const result = await cartService.getOrdersByUserId(1);
    expect(result).toEqual([]);
  });
});
