import Client from "../database";

export type Product = {
    id?: Number;
    name: string;
    price: Number;
}

export class ProductStore {
    async index(): Promise<Product[]> {
      try {
        const sql = "SELECT * FROM products";
        const conn = await Client.connect();
  
        const result = await conn.query(sql);
  
        conn.release();
  
        return result.rows;
      } catch (err) {
        throw new Error(`${err}`);
      }
    }

    async show(id: string): Promise<Product> {
        try {
          const sql = "SELECT * FROM products WHERE id=($1)";
          const conn = await Client.connect();
    
          const result = await conn.query(sql, [id]);
    
          conn.release();
    
          if (!result.rowCount) {
            throw `Could not find product ${id}.`;
          }
    
          return result.rows[0];
        } catch (err) {
          throw new Error(`${err}`);
        }
      }
    
      async create(prod: Product): Promise<Product> {
        try {
          const sql = "INSERT INTO products (name, price) VALUES($1, $2) RETURNING *";
          const conn = await Client.connect();
    
          const result = await conn.query(sql, [prod.name, prod.price]);
    
          const order = result.rows[0];
    
          conn.release();
    
          return order;
        } catch (err) {
          throw new Error(`Could not add new product. Error: ${err}`);
        }
      }
}