import Client from "../database";

export type Order = {
  id?: Number;
  userId: Number;
  status: string;
};

export type OrderProducts = {
  id: Number;
  quantity: Number;
  orderId: Number;
  productId: Number;
};

export class OrderStore {
  async show(id: string): Promise<Order> {
    try {
      const sql = "SELECT * FROM orders WHERE id=($1)";
      const conn = await Client.connect();

      const result = await conn.query(sql, [id]);

      conn.release();

      if (!result.rowCount) {
        throw `Could not find order ${id}.`;
      }

      return result.rows[0];
    } catch (err) {
      throw new Error(`${err}`);
    }
  }

  async create(o: Order): Promise<Order> {
    try {
      const sql = "INSERT INTO orders (user_id, status) VALUES($1, $2) RETURNING *";
      const conn = await Client.connect();

      const result = await conn.query(sql, [o.userId, o.status]);

      const order = result.rows[0];

      conn.release();

      return order;
    } catch (err) {
      throw new Error(`Could not add new order. Error: ${err}`);
    }
  }
}
