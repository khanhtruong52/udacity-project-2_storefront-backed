import jwt from "jsonwebtoken";
import { Request, Response } from "express";

const jwtValidator = (req: Request, res: Response, next: () => void) => {
  try {
    const authorizationHeader = req.headers.authorization;
    const token = authorizationHeader?.split(" ")[1];
    
    jwt.verify(token, process.env.JWT_SECRET_KEY);

    next();
  } catch (err) {
    res.status(401).json("Access denied, invalid token");
  }
};

export default jwtValidator;
