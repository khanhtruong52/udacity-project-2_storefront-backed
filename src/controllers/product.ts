import express, { Request, Response } from "express";
import { Product, ProductStore } from "../models/product";
import jwtValidator from "../middlewares/jwtValidator";

const products = express.Router();
const store = new ProductStore();

const index = async (req: Request, res: Response) => {
  try {
    const result = await store.index();
    res.json(result);
  } catch (err) {
    res.status(500).json(err);
  }
};

const show = async (req: Request, res: Response) => {
  try {
    const product = await store.show(req.params.id);
    res.json(product);
  } catch (error) {
    res.status(404).json(error);
  }
};

const create = async (req: Request, res: Response) => {
  const product: Product = {
    name: req.body.name,
    price: req.body.price,
  };

  try {
    const newProduct = await store.create(product);
    res.json(newProduct);
  } catch (err) {
    res.status(500).json(err);
  }
};

products.get("/", index);
products.get("/:id", show);
products.post("/", jwtValidator, create);

export default products;
