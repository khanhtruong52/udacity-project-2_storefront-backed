import express, { Request, Response } from "express";
import { Order, OrderStore } from "../models/order";
import { CartService } from "../services/cart";

const orders = express.Router();
const store = new OrderStore();
const cartService = new CartService();

const show = async (req: Request, res: Response) => {
  if (isNaN(Number(req.params.id))) {
    res.status(400).end("Order Id must be numeric.");
    return;
  }

  await store
    .show(req.params.id)
    .then((order) => {
      res.status(200).json(order);
    })
    .catch((err) => {
      res.status(500).end(err.message);
    });
};

const create = async (req: Request, res: Response) => {
  if (isNaN(Number(req.body.userId))) {
    res.status(400).end("userId must be numeric.");
    return;
  }

  const order: Order = {
    userId: req.body.userId,
    status: "active"
  };

  await store
    .create(order)
    .then((order) => {
      res.status(200).json(order);
    })
    .catch((err) => {
      res.status(500).end(err.message);
    });
};

const addProduct = async (req: Request, res: Response) => {
  const orderId: number = Number(req.params.id);
  const productId: number = Number(req.body.productId);
  const quantity: number = Number(req.body.quantity);

  if(isNaN(orderId)){
    res.status(400).end("Order ID is invalid");
    return;
  }
  if(isNaN(productId)){
    res.status(400).end("Product ID is invalid");
    return;
  }
  if(isNaN(quantity)){
    res.status(400).end("Quantity ID is invalid");
    return;
  }

  try {
    const addedProduct = await cartService.addToCart(quantity, orderId, productId);
    res.json(addedProduct);
  } catch (err) {
    res.status(500).json(err);
  }
};

const getOrdersByUserId = async (req: Request, res: Response) => {
  const userId = Number(req.params.id);

  if(isNaN(userId)){
    res.status(400).end("User ID is invalid");
    return;
  }

  try {
    const result = await cartService.getOrdersByUserId(userId);
    res.json(result);
  } catch (err) {
    res.status(500).json(err);
  }
};

orders.get("/:id", show);
orders.get("/user/:id", getOrdersByUserId);
orders.post("/", create);
orders.post("/:id/product", addProduct);

export default orders;
