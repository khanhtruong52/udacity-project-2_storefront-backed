import app from "../server";
import supertest from "supertest";
import jwt from "jsonwebtoken";
import { ProductStore } from "../models/product";

const store = new ProductStore();

describe("Product Endpoints suite test", () => {
  let request;
  const token = "Bearer " + jwt.sign({ user: {} }, process.env.JWT_SECRET_KEY);

  beforeAll(() => {
    request = supertest(app);
  });

  it("endpoint test for index", async () => {
    const response = await request.get("/products");
    expect(response.status).toBe(200);
  });

  it("endpoint test for create", async () => {
    const response = await request
      .post("/products")
      .send({
        name: "Valorant",
        price: 0,
      })
      .set("authorization", token);
    expect(response.status).toBe(200);
  });

  it("endpoint test for show", async () => {
    const response = await request.get("/products/1");
    expect(response.status).toBe(200);
  });
});

describe("Product Model suite test", () => {
  it("should have an index method", () => {
    expect(store.index).toBeDefined();
  });

  it("should have an show method", () => {
    expect(store.show).toBeDefined();
  });

  it("should have an create method", () => {
    expect(store.create).toBeDefined();
  });

  it("create method should add a product", async () => {
    const result = await store.create({
      name: "dota2",
      price: 0,
    });

    expect(result).toEqual({
      id: 2,
      name: "dota2",
      price: 0,
    });
  });

  it("index method should return a list of products", async () => {
    const result = await store.index();
    expect(result[1]).toEqual(
      {
        id: 2,
        name: "dota2",
        price: 0,
      },
    );
  });

  it("show method should return the correct product", async () => {
    const result = await store.show("2");
    expect(result).toEqual({
      id: 2,
      name: "dota2",
      price: 0,
    });
  });
});
