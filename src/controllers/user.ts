import express, { Request, Response } from "express";
import jwt from "jsonwebtoken";
import { User, UserStore } from "../models/user";
import jwtValidator from "../middlewares/jwtValidator";

const users = express.Router();
const store = new UserStore();

const index = async (_req: Request, res: Response) => {
  try {
    const result = await store.index();
    res.json(result);
  } catch (err) {
    res.status(500).json(err);
  }
};

const show = async (req: Request, res: Response) => {
  try {
    const user = await store.show(req.params.id);
    res.json(user);
  } catch (error) {
    res.status(404).json(error);
  }
};

const create = async (req: Request, res: Response) => {
  const user: User = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    password: req.body.password,
  };

  try {
    const newUser = await store.create(user);
    var token = jwt.sign({ user: newUser }, process.env.JWT_SECRET_KEY);
    res.json(token);
  } catch (err) {
    res.status(500).json(err);
  }
};

users.get("/", jwtValidator, index);
users.get("/:id", jwtValidator, show);
users.post("/", create);

export default users;
